---
title: "Data Manipulation"
author: "Antoine Lamer"
date: "09 septembre 2020"
output:
  html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)

library(tidyr)
library(dplyr)
```


## Working directory

<style>
body { text-align: justify, font-size: 10px !important;}
h2 { font-size: 30px !important;  }
p { font-size: 14px !important; }
ul { font-size: 14px !important; }
pre { font-size: 14px !important; }
</style>

**getwd** : get the actual working directory

```{r working_directory}
getwd()
```

**setwd** : set a new working directory

With Rmarkdown, the working directory is the directory in which the Rmarkdown is stored.

## Loading data

One function for each kind of data:

* read.table
* read.csv (or read.csv2)
* read.xlsx
* download.file()
* xmlTreeParse(), xmlRoot()
* fromJSON()
* dbConnect(), dbGetQuery(), dbReadTable()
* …

## Loading data

```{r eval = FALSE}
read.csv2(file, header = TRUE, sep = ";", quote = "\"", dec = ",", fill = TRUE, comment.char = "", ...)
```

```{r data_reading, cache = TRUE}
file_src = read.csv2("data.csv", sep=';', na.strings=c("","NA"), encoding = 'UTF8')
load('../data/biology_results.RData')
```

## Data summary

```{r data_summary, dependson="data_reading", cache= TRUE}

head(summary(file_src))

head(str(file_src))

```

## Data cleaning

```{r data_management, dependson="data_summary", cache= TRUE}

data_src = file_src

colnames(data_src) = tolower(colnames(data_src))

data_src$asa = factor(data_src$asa)
levels(data_src$asa) = c("ASA1", "ASA2", "ASA3", "ASA4", "ASA5")

##data_src$mois_fr = ordered(data_src$mois_fr, levels=c("Jan", "Fév", "Mar", "Avr", "Mai", "Jun", "Jui", "Aou", "Sep", "Oct", "Nov", "Dec"))

## Fin du data management

data = data_src

```

## Filtering

```{r , dependson="data_summary"} 

data_min = data %>%
        select(c(age, poids, taille, asa, sexe, trimestre_annee, annee, service,duree_anesthesie, lib_chapitre, duree_sejour_interv, deces_sejour_interv, rea_si_post_op, nb_fcecg_chir, moy_fcecg_chir,      nb_pam_chir, moy_pam_chir, nb_spo2_chir, moy_spo2_chir)) %>%
        filter(!is.na(age)) %>% 
        filter(!is.na(poids)) %>% 
        filter(!is.na(taille)) %>%
        filter(!is.na(asa)) %>%
        filter(!is.na(sexe)) %>%
        filter(duree_sejour_interv < 30) %>%
        filter(trimestre_annee == 'T1_2011')
```

## Subsetting

```{r subs_1, eval = FALSE}

## all rows, first column
head(data_min[,1])

## all rows, column age
head(data_min[,"age"])

## all rows, columns 1 and 2
head(data_min[,c(1,2)])

## all rows, columns 1 to 5
head(data_min[,1:5])

## all rows, all columns except the first
head(data_min[,-1])

## second row, all columns
head(data_min[2,])
```

## Subsetting

* dataset[*condition*,] uses a boolean vector

* dataset[which(*condition*)] uses a vector of indices corresponding to rows satisfying *condition*

* subset(dataset, *condition*, select = column)

```{r subs_2}

# Return a boolean vector
df1 = data_min[(data_min$service == 'CCV' & data_min$age == 50),]

# Return a vector of indices
df2 = data_min[which(data_min$service == 'CCV' & data_min$age == 50),]

df3 = subset(data_min, service == 'CCV' & age == 50)

```

## Adding new column

```{r add_new_column, cache=TRUE, dependson='data_management'}

data_min$new_variable = rnorm(nrow(data_min))

# cbind

new_variable2 = rnorm(nrow(data_min))

data_min = cbind(data_min, new_variable2)

```

## Sorting

* **sort** returns ordered values

```{r sort}

df_ccv = data_min[(data_min$service == 'CCV' & data_min$age == 50),]

# returns weight value ordered
sort(df_ccv$poids)

# returns decreasing weight values
sort(df_ccv$poids, decreasing=TRUE)
```

## Sorting

* **order** returns ordered indices

```{r order, eval = TRUE}

df_ccv = data_min[(data_min$service == 'CCV' & data_min$age == 50),]

# return indices of rows ordered according weight values
order(df_ccv$poids)

# return rows ordered according weight values
df_ccv[order(df_ccv$poids),]
```

## Sorting

* **order** returns ordered indices

```{r order_2, eval = TRUE}

# return rows ordered according weight values and asa status
df_ccv[order(df_ccv$poids, df_ccv$asa),]
```


## tidyr - pivot_wider and pivot_longer

<center>
<img src="../images/biology_pivot.png" align="middle" alt="drawing" style="max-width:80%;"/>
</center>

* **pivot_wider** transforms columns to rows
* **pivot_longer** transforms rows to columns

```{r}
# Date index pour le pivot
# ----------------------------------------------------------------------------

biology$index_date = as.numeric(difftime(biology$biology_date, biology$event_date, units = 'days'))

# Filtre pour la largeur maximale
# ----------------------------------------------------------------------------

biology = biology %>%
  filter(index_date <= 5)

# Agrégation
# ----------------------------------------------------------------------------

biology_agg = biology %>%
  dplyr::select(-c(biology_date, biology_hour, event_date)) %>%
  dplyr::group_by(patient_id, index_date, biology_id) %>%
  dplyr::summarise(min = min(biology_result),
                   max = max(biology_result))

```

### pivot_wider

```{r}
# Pivot wider
# ----------------------------------------------------------------------------

biology_wider = biology_agg %>%
  pivot_wider(names_from = c(biology_id, index_date),
              values_from = c(min, max))

biology_wider
```

### pivot_longer

```{r}
# Pivot longer
# ----------------------------------------------------------------------------

biology_longer = biology_wider %>%
  pivot_longer(!c(patient_id), names_to = "biology_id", values_to = "count")

biology_longer
```

## merge

```{r}
unit = data.frame(unit_id = c(1,2,3), 
                  unit = c("Emergency", "Obstetrics", "Traumatology"))
unit

intervention = data.frame(intervention_id = 1:5, 
                          date = (Sys.Date() + sample(1:10, 5, replace=TRUE)), 
                          unit_id = sample(1:3, 5, replace=TRUE))
intervention
```

## merge

**merge** function is equivalent to *join* operations in database.
Have a look at following arguments :

* by = intersect(names(x), names(y))
* by.x = by
* by.y = by
* all = FALSE
* all.x = all
* all.y = all

```{r}
merge(intervention, unit, by = c("unit_id"))
```

## apply / tapply / lapply / mapply

* **apply** evaluates a function over an array 
* **lapply** evalues a function over a list
* **sapply** evalues a function over a list and try to simplify the result (in a vector, a matrix or a list)
* **tapply** evalues a function over subsets of a vector (combination of split() and sapply())
* **mapply** evalues a function over subsets of a vector

## lapply

* **lapply** evalues a function over a list

```{r}

# mean of each element of the list
l = list(1:5, 1:10, 10:20)
lapply(l, mean)

```

## sapply

```{r}
# mean of each element of the list 
l = list(1:5, 1:10, 10:20)
sapply(l, mean)
```

## sapply

* **sapply** evalues a function over a list and try to simplify the result (in a vector, a matrix or a list)

```{r}
# mean of each element of the list 
s = split(biology$biology_result, biology$biology_id)

s
sapply(s, mean)
```

## tapply

* **tapply** evalues a function over subsets of a vector (combination of split() and sapply())

```{r}

# Mean of value for each variable
tapply(biology$biology_result, biology$biology_id, mean) 

```

## unlist

* **unlist** flattens a list

```{r}

l = list(c(2:5), rep('A', 4))
ul = unlist(l)
str(ul)

```

## Working with characters

* **tolower**  converts upper-case characters to lower-case characters

* **toupper** converts lower-case characters to upper-case characters

* **paste** by defaut, concatanates two strings separated by " "

* **paste0** is equivalent to paste(..., sep = "")

* **nchar** returns the number of characters in a string

```{r}
paste("1st diagnosis", "2nd diagnosis", sep = ", ")
```

## Working with characters

* **strsplit** splits the elements of a character vector into substrings

* **grep** searches for matches to argument pattern within each element of a character vector and returns indices
* **grepl** returns a logical vector

```{r}
strsplit("first element-second element-third_element", "-")

grep("a", c("a", "b", "c", "a"))

grepl("a", c("a", "b", "c", "a"))
```

## Working with characters

* **subtr** extracts or replaces substrings in a character vector

* **str_trim** trims character strings to specified display widths.

* **sub** performs replacement for the first match

* **gsub** performs replacement for all matches

```{r eval=FALSE}
diag_src = 'Diagnosis: Essential (primary) hypertension'

substr(diag_src, 12, nchar(diag_src))

head(gsub('_', '', data_min$trimestre_annee))
```

## Regular expressions

* A **regular expression** is a sequence of characters that defines a search pattern:

**^** represents the start of a line
**$** represents the end of a line
**[Aa]** represents a set of characters
**[A-Z]**, **[a-z]** and **[0-9]** represent a range of letters or figures

```{r}

head(grep("[Ii][0-9][0-9]", data_min$lib_chapitre))
head(grepl("[Ii][0-9][0-9]", data_min$lib_chapitre))

head(data_min[grep("[Ii][0-9][0-9]", data_min$lib_chapitre),c("lib_chapitre")])
```

## Dates

```{r}
Sys.timezone()

Sys.getenv('TZ')

Sys.setenv(TZ="Europe/Paris")

Sys.time()

```

## Dates 

* **date()** returns the current date as characters

* **Sys.Date()** returns the current date as an object Date

```{r}
d1 = date()
class(d1) ; d1

d2 = Sys.Date()
class(d2) ; d2
```

## Dates

* **Sys.Date()** returns the current date as an object Date

```{r}
d2 = Sys.Date()
d2 + 1
d2 - 1
weekdays(d2)
months(d2)
```

## Formatting dates

* **format**(date, format)  

```{r}
d2 = Sys.Date()
format(d2, '%d/%m/%y')
```
<center>
<img src="../images/format_date.png" align="middle" alt="drawing" style="max-width:60%;"/>
</center>

## as.Date()

* **as.Date()** converts characters to date

```{r}
# convert date info in format 'mm/dd/yyyy'
strDates <- c("01/05/1965", "08/16/1975")
class(strDates)
dates <- as.Date(strDates, "%m/%d/%Y")
dates
```

## Lubridate 

```{r eval=FALSE}

library(lubridate)

dmy("30042016")

dmy_hms("30042016 21:04:48")

```

## Export

Exporter un graphique déjà existant :

* dev.print

```{r export_hist_png, fig.width=100, eval=FALSE}

hist(data_min$duree_sejour_interv, main = "Length stay")

dev.print(device = png, file = "export/export_hist_stay_length.png", width = 600)

```

## Export

Rediriger la sortie graphique vers un fichier, puis exécuter la commande produisant la figure.

Fonctions png, jpeg et tiff for bitmap formats, and svg, pdf, postscript and win.metafile for vectoriel formats.

```{r export_hist_dev, fig.width=100, eval=FALSE}

jpeg(file = "export/export_hist_stay_length.jpeg", width = 800, height = 700)
hist(data_src$duree_sejour_interv, main = "Length stay")
dev.off()

pdf(file = "export/export_length_stay.pdf", width = 9, height = 9, pointsize = 10)
hist(data_src$duree_sejour_interv, main = "Length stay")
barplot(table(data_src$mois_fr), main = "Interventions per month")
dev.off()
```