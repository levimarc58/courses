---
title: "R programming"
author: "Antoine Lamer"
date: "06 septembre 2020"
output:
  html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE, comment=NA)
```

## R Language

### Objects

- character
- complex
- logical
- null
- numeric

```{r r_objects, eval = FALSE}
logic = TRUE
mode(logic)
logical(logic)
as.numeric(logic)
```

### NA

Missing value NA ‘Not Available’

```{r na}
x = NA
print(x)
print(x+1)
is.na(x)
```

### Assignment operators

<- value
value -> x
x = value

```{r}
x <- 3
x
4 -> y
y
z = 5
z
```

### Arithmetic operators

\+ 
\- 
\* 
\/

```{r}
  3+4
(7-2)*2 
```

### Logical operators

& (et)

| (ou)

```{r}
FALSE & TRUE
FALSE | TRUE
```

### Comparison operators
\< 

\<\= 

\> 

\>\= 

\=\= 

\!\=

```{r}
FALSE & TRUE
FALSE | TRUE
```

### Exercices

```{r eval = FALSE}

10>(3*2)

a = 2
b = 3

a < b

c = a < 3
d = b > 4
c & d
c | d 
```

### Data Structures

| Homogeneous | Heterogeneous | Number of dimensions |
| -------- | -------- | -------- |
| Vector    | List | 1     |
| Matrix    | Data frame     | 2     |
| Array    | nb_deces (deces_src) / nb_habitants (pop_age_ville_src)     | n     |

Note : Individual objects are vectors of length 1

#### R Vectors
  
sequence of data of the same type

```{r}
c(1, 2, 3)
1:4
seq(1,5,by=0.5)
seq(1,4,length=7)
```

**Operations with vectors :**

```{r}
v = c(3, 10, 5)
v + 10
v < 6
```

**Extraction**

```{r}
v[2]  			# second element
v[c(2,3)]			# second and thirst element 
v[-1]				# vector minus first element
v[v<8]			# elements inferior to 8
```

**find locations**

```{r}
which(v==10)
```

**replace value**

```{r}
v[which(v==10)] = 9
v
```

**logical operations**

```{r}
x = c(1, 2, 3)
x < 2
```

**all** return TRUE if all of the elements satisfy the condition
**any** return TRUE if at least one of the elements satisfies the condition

```{r}
all(x<2)
any(x>2)
```

#### Factors

Factors are vectors adapted to qualitative data

```{r}
sex = factor(c('M', 'M', 'F', 'F', 'F', 'M'))
sex

level = ordered(c("Low", "Medium", "Medium", "High"), levels=c("Low", "Medium", "High"))
level

f = as.factor(c(1, 1, 2, 1, 2, 3))
f
```

#### R Matrix

data of the same type
each element is characterized by its number of line and its number of column

```{r}
m = matrix(c(1,2,3,4,5,6), ncol=3)
m

m = matrix(1:4, nrow=2)
m

m[1,2]
m[1,]
m[,2]
which(m==3)
which(m==3, arr.ind=TRUE)

dim(m)
length(m)
mode(m)
nrow(m)
ncol(m)

cbind(c(0, 1), c(2, 3))
rbind(c(0, 1), c(2, 3))
```

Exemple :

```{r}
m = cbind(c(132, 166, 167), c(3056, 3196, 3392))
m
rownames(m) = c('2014', '2015', '2016')
m
colnames(m) = c('D', 'V')
mode(m)
class(m)
prop.table(m, 1)
round(prop.table(m,1),2)
```

#### Lists

heterogeneous ensemble of data
return of functions

```{r}
regl = lm(Petal.Width ~ Petal.Length, data=iris)
regl
str(regl)
regl[[1]]
regl$coefficients
regl[['coefficients']]
```

Exemple :

```{r}
l = list(1:5, "a", "b", "c", TRUE, 5.5, "string 1")
l
```

#### Array

can store data in more than two dimensions
store only one type
array1[1st dimension, 2nd dimension, 3rd dimension]

```{r}
m = matrix(1:6,2)
m
```

#### Dataframe

Lists with same length, different modes

```{r}
v1 = 1:10
v2 = factor(c('M', 'M', 'F', 'F', 'F', 'M', 'F', 'F', 'F', 'M'))
df = data.frame(variable1 = v1, variable2 = v2)
df
```



### Conditions

**if ...**

```{r}

age = 16

if(age < 18) {
  print("Minor")
}
```

**if ... else ...**

```{r}
age = 30

if(age < 18) {
  print("Minor")
} else {
  print("Major")
}
```

**if ... 
else if ... 
else ...**

```{r}
age = NA

if(is.na(age)) {
  print("Age undefined")
} else if (age < 18) {
  print("Minor")
} else {
  print("Major")
}
```

### Loop

**for(...)**

```{r}
for(i in 1:5) {
	print(i)
}
```

**while (...)**

```{r}
i = 0
while(i < 5) {
	print(i)
	i = i + 1
}
```
