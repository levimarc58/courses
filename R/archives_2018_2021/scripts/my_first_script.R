#########################################
#
# Exercice : Manipulation des fichiers INSEE + vote + vaccination
# Objetifs :
# - data management
#
# Date de création : kddkd
# Date de dernière modification :
# Auteur : 
#
#########################################

## Chargement des bibliothèques

## Chemins
path_raw_data = "/media/ant/data_crypt/bases_nationales/ressources/data"
path_export_data = "/media/ant/data_crypt/01_projets/bases_nationales/votes_vaccination/data"

list.files(path_raw_data)

## Notes

# 20 juin 2021 pour le 1er tour
# 27 juin 2021 pour le second tour

# 1.1) Chargement des données
##########################################################################

# Votes
# chargement du fichier avec read.csv2
# https://gitlab.univ-lille.fr/master_dss/datalib/-/tree/master/data/vaccination
vaccination_src = read.csv2(file.path(path_raw_data, "donnees-de-vaccination-par-epci.csv"), 
                            stringsAsFactors = FALSE, 
                            dec = ".")

str(vaccination_src) # Affiche les noms des variables, leurs types et quelques valeurs

summary(vaccination_src) # Propose quelques statistiques descriptives de chacune des variables

head(vaccination_src) # Affiche les 5 premières lignes

head(vaccination_src, 10) # Affiche les 10 premières lignes

dim(vaccination_src) # Nombre de lignes et nombres de colonnes

# Manipulation des données

vaccination_src$semaine_injection # Accéder à une variable

vaccination_src[40,] # Afficher la quarantième ligne

vaccination_src[,2] # Afficher la deuxième colonne

vaccination_src[10:20,] # Afficher les lignes 10 à 20

vaccination_src[c(1, 2, 4),] # Afficher les lignes 1, 2, 4

vaccination_src[,1] # Afficher la première colonne

vaccination_src[,-1] # Tout afficher sauf la première colonne

vaccination_src[,c('classe_age', 'libelle_classe_age')] # Afficher les colonnes 'classe_age' et 'libelle_classe_age'

vaccination_src[1,2] # Affiche la deuxième colonne de la première ligne

# Conditions
vaccination_src$semaine == '2021-09'
vaccination_src[vaccination_src$semaine == '2021-09',]


vaccination_src[vaccination_src$semaine == '2021-09' & vaccination_src$classe_age == 'TOUT_AGE',]
vaccination_src[vaccination_src$semaine == '2021-09' & vaccination_src$classe_age == 'TOUT_AGE',
                c('libelle_epci', 'taux_cumu_1_inj', 'taux_cumu_termine')]

# Création d'un nouveau dataframe
vaccination_09_tout_age = vaccination_src[vaccination_src$semaine == '2021-09' & vaccination_src$classe_age == 'TOUT_AGE',
                                          c('libelle_epci', 'taux_cumu_1_inj', 'taux_cumu_termine')]
dim(vaccination_09_tout_age)

# Graphiques
hist(vaccination_09_tout_age$taux_cumu_termine)

quantile(vaccination_09_tout_age$taux_cumu_termine)
mean(vaccination_09_tout_age$taux_cumu_termine)
  