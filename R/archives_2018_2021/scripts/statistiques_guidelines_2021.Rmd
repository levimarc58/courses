---
title: "Statistiques Vaccins"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

```{r libraries, include=FALSE}
## Chargement des bibliothèques
library(dplyr)
library(stringr)
```

```{r chemin}
## Chemins
path_import_data = "/media/ant/data_crypt/01_projets/bases_nationales/votes_vaccination/data"
```

# Chargement des données

```{r chargement}
votes_vaccination_src = read.csv2(file.path(path_import_data, "votes_vaccinations_210915.csv"), 
                            stringsAsFactors = FALSE)
str(votes_vaccination_src)
```

# Exploration des données

## Plot vaccination

```{r plot_vaccination}

```

## Plot votes

## Plot vaccination ~ votes

## Plot vaccination ~ âge

## Boxplot votes ~ département
Sélection des départements 59, 62, 93, et 40

# Boxplot Vaccination ~ département



# Statistiques

## Tests bivariés

## Tests multivariés