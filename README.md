# Courses

| Course | Description |  |
| -------- | -------- | -------- |
| Databases    |  Relational databases, modelisation, SQL  | M1 Health Data Science   |
| R     |  R programming, Rmarkdown, packages, graphics     |  M1 Health Data Science  |